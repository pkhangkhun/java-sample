package com.example.demo.controller;

import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@Controller
public class ReportController {

    @GetMapping(value = "/report")
    public ResponseEntity<byte[]> genReport() throws IOException {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("Content-Type", "text/csv");

//        Path path = Paths.get("tmp/result.csv");
        File resource = new ClassPathResource("tmp/result.csv").getFile();
        byte[] data = Files.readAllBytes(resource.toPath());

        return new ResponseEntity<>(data, responseHeaders, HttpStatus.OK);
    }

}
